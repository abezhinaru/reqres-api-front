import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/skip';
import 'rxjs/add/operator/takeUntil';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { UserService } from '../services/user.service';
import * as userActions from '../actions/user';
import UserSearchInfo from "../models/user-search-info";
import UserFilter from "../models/user-filter";


@Injectable()
export class UserEffects {

  constructor(private actions$: Actions, private userService: UserService) { }

  @Effect()
  search$: Observable<Action> = this.actions$
    .ofType(userActions.SEARCH)
    .debounceTime(300)
    .map((action: userActions.SearchAction) => action.payload)
    .switchMap(filter => {

      const nextSearch$ = this.actions$.ofType(userActions.SEARCH).skip(1);

      return this.userService.getUsers(filter)
        .takeUntil(nextSearch$)
        .map(result => new userActions.SearchComplete(result))
        .catch(() => of(new userActions.SearchComplete(new UserSearchInfo())));
    });

  @Effect()
  selectUser$: Observable<Action> = this.actions$
    .ofType(userActions.SELECT_USER)
    .map((action: userActions.SelectUser) => action.payload)
    .switchMap((userId) => {

      const nextSearch$ = this.actions$.ofType(userActions.SEARCH).skip(1);

      return this.userService.getUser(userId)
        .takeUntil(nextSearch$)
        .map(result => new userActions.SetSelectedUser(result));
    });
}
