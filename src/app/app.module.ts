import 'hammerjs';

import { BrowserModule }            from '@angular/platform-browser';
import { NgModule }                 from '@angular/core';
import { HttpModule }               from '@angular/http';
import { FormsModule }              from '@angular/forms';
import { StoreModule }              from '@ngrx/store';
import { EffectsModule }            from '@ngrx/effects';
import {
  StoreRouterConnectingModule
}                                   from "@ngrx/router-store";

import { MatProgressBarModule }     from '@angular/material/progress-bar';
import { MatPaginatorModule }       from '@angular/material';


import { ComponentsModule }         from './components';
import { UserService }              from './services/user.service';
import { reducers }                 from './reducers';

import { FindUserPage }             from './containers/find-user-page';
import { AppComponent }             from './containers/app';
import { UserEffects }              from './effects/user';
import { UserInfoPage }             from './containers/user-info-page';


import { routeModuleProvider }      from './route-provider';
import {RouterEffects} from "./effects/router";



@NgModule({
  declarations: [
    AppComponent,
    FindUserPage,
    UserInfoPage
  ],
  imports: [
    MatProgressBarModule, //#FIXME move this dependencies to './components' module
    MatPaginatorModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    ComponentsModule,
    routeModuleProvider,
    StoreModule.forRoot(reducers),
    StoreRouterConnectingModule.forRoot({ stateKey: 'router' }),
    EffectsModule.forRoot([
      UserEffects, RouterEffects
    ])
  ],
  providers: [
    UserService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
