import { appRoutes } from "../routes";


export interface State {
  routes: {}[];
}

export const initialState: State = {
  routes: appRoutes
};


export const reducer = (state = initialState) => state;


export const getRoutes = (state: State): {}[] => state.routes;
