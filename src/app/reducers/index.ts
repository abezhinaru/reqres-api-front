import * as fromUser from './users';
import * as fromSearch from './search';
import * as fromLayout from './layout';
import * as fromRoutes from './routes';
import { ActionReducerMap } from "@ngrx/store";

import { routerReducer } from '@ngrx/router-store';

import { createSelector } from 'reselect';
import {CustomRoute} from "../routes";

export interface State {
  users: fromUser.State,
  search: fromSearch.State,
  layout: fromLayout.State,
  routeConfig: fromRoutes.State
  router: any;
}

export const reducers: ActionReducerMap<State> = {
  users: fromUser.reducer,
  search: fromSearch.reducer,
  layout: fromLayout.reducer,
  routeConfig: fromRoutes.reducer,
  router: routerReducer
};


export const getUsersState = (state: State): fromUser.State => state.users;
export const getSearchState = (state: State): fromSearch.State => state.search;
export const getRouteConfigState = (state: State): fromRoutes.State => state.routeConfig;
export const getLayoutState = (state: State): fromLayout.State => state.layout;

export const getRoutes = createSelector(getRouteConfigState, fromRoutes.getRoutes);
export const getUserEntities = createSelector(getUsersState, fromUser.getEntities);
export const getSearchIds = createSelector(getSearchState, fromSearch.getIds);
export const getSearchResult = createSelector(getUserEntities, getSearchIds, (users, ids) => ids.map(id => users[id]));
export const getSearchFilter = createSelector(getSearchState, fromSearch.getFilter);
export const getLayoutIsOpen = createSelector(getLayoutState, fromLayout.getIsOpen);
export const getSearchLoading = createSelector(getSearchState, fromSearch.getLoading);

export const getSearchInfo = createSelector(getSearchState, fromSearch.getInfo);

export const getSelectedUserId = createSelector(getUsersState, fromUser.getSelectedUserId);

export const getSelectedUser = createSelector(getUserEntities, getSelectedUserId,
  (userEntities, selectedUserId) => userEntities[selectedUserId]);



