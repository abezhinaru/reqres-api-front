import User from '../models/user';
import * as userActions from '../actions/user';
import {SET_SELECTED_USER} from "../actions/user";

export interface State {
  ids: string[] | number[],
  entities: { [id: string]: User },
  selectedUserId?: number
}

const initialState = {
  ids: [],
  entities: {},
  selectedUserId: null
};

export const reducer = (state = initialState, action) => {
  switch(action.type) {
    case userActions.SEARCH_COMPLETE: {
      const newUsers = action.payload.data.filter(user => !state.entities[user.id]);

      const newUserEntities = newUsers.reduce((result, user) => {
        result[user.id] = user;

        return result;
      }, {});

      const newUserIds = newUsers.map(user => user.id);

      return {
        ids: [...state.ids, ...newUserIds],
        entities: {...state.entities, ...newUserEntities}
      };
    }
    case SET_SELECTED_USER: {
      const user = action.payload;

      if (state.ids.includes(user.id)) {
        return {...state, selectedUserId: user.id};
      } else {
        return {
          ids: [...state.ids, user.id],
          entities: {...state.entities, [user.id]: user},
          selectedUserId: user.id
        };
      }
    }
    default:
      return state;
  }
};


export const getEntities = (state: State) => state.entities;
export const getSelectedUserId = (state: State) => state.selectedUserId;
