import * as userActions from '../actions/user';
import UserFilter from '../models/user-filter';
import UserSearchInfo from "../models/user-search-info";

export interface State {
  ids: number[]
  filter: UserFilter,
  searchInfo: UserSearchInfo
  loading: boolean,
}

export const initialState: State = {
  ids: [],
  filter: new UserFilter(1, 1),
  loading: false,
  searchInfo: new UserSearchInfo()
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case userActions.SEARCH: {
      return {...state,
        filter: action.payload,
        loading: true
      };
    }
    case userActions.SEARCH_COMPLETE: {
      const searchInfo:UserSearchInfo = {
        ...action.payload,
        data: null
      };

      return {
        ids: action.payload.data.map(user => user.id),
        searchInfo,
        filter: state.filter,
        loading: false
      };
    }
    default:
      return state;
  }
};


export const getIds = (state: State) => state.ids;
export const getFilter = (state: State) => state.filter;
export const getLoading = (state: State) => state.loading;
export const getInfo = (state: State) => state.searchInfo;
