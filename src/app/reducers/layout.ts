import * as layoutActions from '../actions/layout';



export interface State {
  isOpen: boolean;
}

export const initialState: State = {
  isOpen: false,
};


export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case layoutActions.OPEN_SIDE_NAV: {
      return { isOpen: true };
    }
    case layoutActions.CLOSE_SIDE_NAV: {
      return { isOpen: false }
    }
    default: {
      return state;
    }
  }
};


export const getIsOpen = (state: State) => state.isOpen;
