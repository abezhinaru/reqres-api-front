import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from "rxjs/Observable";

import UserFilter from '../models/user-filter';
import UserSearchInfo from '../models/user-search-info';



import User from "../models/user";

@Injectable()
export class UserService {
  API_ROOT: string = 'https://reqres.in/api';

  constructor(private http: Http) {}

  getUsers(search: UserFilter): Observable<UserSearchInfo> {
    const uri = `${this.API_ROOT}/users`;

    return this.http.get(uri, { params: search })
      .map(result => result.json());
  }

  getUser(userId): Observable<User> {
    const uri = `${this.API_ROOT}/users/${userId}`;

    return this.http.get(uri)
      .map(result => result.json())
      .map((resp) => resp.data);
  }
}
