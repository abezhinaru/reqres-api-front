import {Component, ViewEncapsulation} from '@angular/core';
import { ActivatedRoute }   from "@angular/router";
import * as fromRoot        from '../reducers';
import { Store }            from "@ngrx/store";

import * as userActions     from '../actions/user';
import { getSelectedUser }  from "../reducers";
import User from "../models/user";
import {Observable} from "rxjs/Observable";


@Component({
  encapsulation: ViewEncapsulation.None,
  host: {
    'class': 'user-info-page',
  },
  styles: [`
    .user-info-page {
      display: flex;
    }
    
    user-card {
      flex-basis: 250px;
      margin: 15px;
    }
  `],
  selector: 'user-info-page',
  template: ` <!--#FIXME without *ngIf angular print error to console...-->
    <user-card *ngIf="user$ | async" [user]="user$ | async "></user-card>
  `
})
export class UserInfoPage {
  user$: Observable<User>;

  constructor(private store: Store<fromRoot.State>, private route: ActivatedRoute) {
    route.params.subscribe(params => this.store.dispatch(new userActions.SelectUser(+params.id)));

    this.user$ = store.select(getSelectedUser);
  }
}
