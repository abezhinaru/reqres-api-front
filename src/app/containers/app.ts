import 'rxjs/add/operator/filter'
import 'rxjs/add/operator/do'

import { Observable }  from "rxjs/Observable";

import { Component }   from '@angular/core';
import { Store }       from "@ngrx/store";

import {CustomRoute} from "../routes";
import * as RouterActions from '../actions/router';
import * as fromRoot from '../reducers/index';
import * as layoutAction from '../actions/layout';


@Component({
  selector: 'app-root',
  template: `
    <layout>
      
      <side-nav [open]="isOpenSideNav$ | async">
        <nav-item *ngFor="let route of routes$ | async" (activate)="closeSideNav()" [routerLink]="route.path" [icon]="route.icon">
          {{route.label}}
        </nav-item>
      </side-nav>
      
      <toolbar (onNavigationChange)="onNavigationHandler($event)" [navItems]="routes$ | async" (openMenu)="openSideNav()"></toolbar>
      
      <router-outlet></router-outlet>
    </layout>
  `
})
export class AppComponent {
  isOpenSideNav$: Observable<boolean>;
  routes$: Observable<CustomRoute[]>;

  constructor(private store: Store<fromRoot.State>) {
    this.isOpenSideNav$ = store.select(fromRoot.getLayoutIsOpen);

    this.routes$ = store.select(fromRoot.getRoutes)
      .map((routes: CustomRoute[]) => routes.filter(route => route.navItemName));
  }


  closeSideNav() {
    this.store.dispatch(new layoutAction.SideNavClose());
  }

  openSideNav() {
    this.store.dispatch(new layoutAction.SideNavOpen());
  }

  onNavigationHandler(route: CustomRoute) {
    this.store.dispatch(new RouterActions.Go({
      path: [route.path],
      extras: { replaceUrl: false }
    }));
  }
}

