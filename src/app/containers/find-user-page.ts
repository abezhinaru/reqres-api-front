import {Component} from '@angular/core';
import { Store } from '@ngrx/store';
import {Router} from "@angular/router";

import {Observable} from 'rxjs/Observable';

import 'rxjs/add/operator/do';


import * as fromRoot from '../reducers';
import * as userActions from '../actions/user';
import User from '../models/user';
import UserFilter from "../models/user-filter";
import UserSearchInfo from "../models/user-search-info";
import * as RouterActions from "../actions/router";



@Component({
  selector: 'find-user',
  styles: [`    
    .container {
      overflow: auto;
      height: calc(100% - 57px - 60px);
    }
    
    .pagination {
      position:absolute;
      bottom:0;
      width:100%;
      height: 57px;
    }
  
  `],
  template: `
    <div class="container">
      <loader [loading]="loading$ | async"></loader>


      <button (click)="listReload()">Reload</button>
      
      <user-preview-list (onUserClick)="onUserClickHandler($event)" [users]="users$ | async "></user-preview-list>
    </div>
    <div class="pagination">
      <mat-paginator (page)="onPaginatorChange($event)" 
                     [length]="(searchInfo$ | async).total_pages"
                     [pageSize]="(searchInfo$ | async).per_page"
                     [pageSizeOptions]="[1, 5, 12]">
      </mat-paginator>
    </div>
    
  `
})
export class FindUserPage {
  users$: Observable<User[]>;
  filter$: Observable<UserFilter>;
  loading$: Observable<boolean>;

  searchInfo$: Observable<UserSearchInfo>;

  constructor(private store: Store<fromRoot.State>) {
    this.users$ = store.select(fromRoot.getSearchResult);
    this.filter$ = store.select(fromRoot.getSearchFilter);
    this.loading$ = store.select(fromRoot.getSearchLoading).map(val => !val);
    this.searchInfo$ = store.select(fromRoot.getSearchInfo);

    this.listReload();
  }

  onUserClickHandler(user: User) {
    this.store.dispatch(new RouterActions.Go({
      path: ['/users', user.id ],
      extras: { replaceUrl: false }
    }));
  }

  onPaginatorChange(data) {
    this.store.dispatch(new userActions.SearchAction(new UserFilter(data.pageSize, data.pageIndex + 1)));
  }

  listReload() {
    this.store.dispatch(new userActions.SearchAction(new UserFilter(5, 0)));
  }
}
