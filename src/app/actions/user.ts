import { Action } from '@ngrx/store';
import UserFilter from "../models/user-filter";
import UserSearchInfo from '../models/user-search-info';
import User from "../models/user";

export const SEARCH = 'USER_SEARCH';
export const SEARCH_COMPLETE = 'USER_SEARCH_COMPLETE';

export const SELECT_USER = 'USER_SELECT';
export const SET_SELECTED_USER = 'SET_SELECTED_USER';

export class SearchAction implements Action {
  readonly type: string = SEARCH;

  constructor(public payload: UserFilter = null) {}
}

export class SearchComplete implements Action {
  readonly type: string = SEARCH_COMPLETE;

  constructor(public payload: UserSearchInfo) {}
}

export class SelectUser implements Action {
  readonly type: string = SELECT_USER;

  constructor(public payload: number) {}
}


export class SetSelectedUser implements Action {
  readonly type: string = SET_SELECTED_USER;

  constructor(public payload: User) {}
}


