import { Action } from '@ngrx/store';

export const CLOSE_SIDE_NAV = 'SIDE_NAV:CLOSE';
export const OPEN_SIDE_NAV = 'SIDE_NAV:OPEN';


export class SideNavClose implements Action {
  readonly type: string = CLOSE_SIDE_NAV;
}

export class SideNavOpen implements Action {
  readonly type: string = OPEN_SIDE_NAV;
}
