import { RouterModule } from "@angular/router";
import { UserInfoPage } from "./containers/user-info-page";
import { FindUserPage } from "./containers/find-user-page";
import {appRoutes} from "./routes";


const componentsByPath = {
  'users': FindUserPage,
  'users/:id': UserInfoPage
};

export const routeModuleProvider = RouterModule.forRoot(
  appRoutes.map(route =>
    ({...route, component: componentsByPath[route.path]})), { enableTracing: true }
);
