import {Component, EventEmitter, Input, Output, ViewEncapsulation} from '@angular/core';
import User from "../models/user";


@Component({
  selector: 'user-card',
  encapsulation: ViewEncapsulation.None,
  styles: [`
    mat-card-title {
      font-size: 20px;
    }
  `],
  template: `
    <mat-card (click)="onClickHandler()">
      <mat-card-title>{{user.first_name}} {{user.last_name}}</mat-card-title>
      <img mat-card-image [src]="user.avatar" />
    </mat-card>
  `
})
export class UserCardComponent {
  @Input() user: User;
  @Output() onCardClick = new EventEmitter<User>();

  onClickHandler() {
    this.onCardClick.emit(this.user);
  }
}
