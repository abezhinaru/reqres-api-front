import {Component, EventEmitter, Input, Output, ViewEncapsulation} from '@angular/core';
import User from "../models/user";


@Component({
  selector: 'user-preview-list',
  encapsulation: ViewEncapsulation.None,
  host: {
    'class': 'user-preview-list',
  },
  styles: [`
    .user-preview-list {
      display: flex;  
      flex-wrap: wrap;
      justify-content: center;
    }

    user-card {
      flex-basis: 250px;
      margin: 15px;
    }
  `],
  template: `    
      <user-card (onCardClick)="onUserClick.emit($event)" *ngFor="let user of users" [user]="user"></user-card>
      <mat-divider></mat-divider>
  `
})
export class UserCardListComponent{
  @Input() users: User[];
  @Output() onUserClick = new EventEmitter<User>();
}
