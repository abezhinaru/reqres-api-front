import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

// MATERIAL IMPORTS;
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatIconModule} from '@angular/material/icon';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatProgressBarModule} from "@angular/material/progress-bar";

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
// MATERIAL COMPONENTS
import { SideNavComponent } from './material/side-nav';
import { NavItemComponent } from './material/nav-item';
import { LayoutComponent } from "./material/layout";
import { ToolbarComponent } from './material/toolbar';
import { LoaderComponent } from "./material/loader";


import { UserCardComponent } from "./user-card";
import { UserCardListComponent } from './user-card-list';
import { UserSearch } from './user-search';


export const COMPONENTS = [
  UserCardListComponent,
  UserSearch,
  SideNavComponent,
  NavItemComponent,
  LayoutComponent,
  ToolbarComponent,
  UserCardComponent,
  LoaderComponent
];


const MATERIAL_MODULES = [
  MatProgressBarModule,
  MatDividerModule,
  MatListModule,
  MatSidenavModule,
  MatIconModule,
  MatToolbarModule,
  BrowserAnimationsModule,
  MatButtonModule,
  MatCardModule
];



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ...MATERIAL_MODULES
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS
})
export class ComponentsModule { }
