import { Component, Input } from '@angular/core';

@Component({
  selector: 'side-nav',
  template: `
    <mat-sidenav [opened]="open">
      <mat-nav-list>
        <ng-content></ng-content>
      </mat-nav-list>
    </mat-sidenav>
  `,
  styles: [`
    md-sidenav {
      width: 300px;
    }
  `]
})
export class SideNavComponent {
  @Input() open = false;
}
