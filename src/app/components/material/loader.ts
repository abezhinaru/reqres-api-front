import {Component, Input} from "@angular/core";


@Component({
  selector: 'loader',
  template: `
    <div [hidden]="loading">
      <mat-progress-bar [mode]="mode"></mat-progress-bar>
    </div>
  `
})
export class LoaderComponent {
  static modes = {
    determinate: 'determinate',
    indeterminate: 'indeterminate',
    buffer: 'buffer',
    query: 'query'
  };

  @Input() loading: boolean;
  @Input() mode: string = LoaderComponent.modes.indeterminate;

}
