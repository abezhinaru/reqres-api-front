import {Component, Output, EventEmitter, Input} from '@angular/core';
import {CustomRoute} from "../../routes";

interface NavItem {
  navItemName: string
}

@Component({
  selector: 'toolbar',
  template: `
    <mat-toolbar color="primary">
      <button mat-icon-button (click)="openMenu.emit()">
        <mat-icon>menu</mat-icon>
      </button>

      <div class="button-row" *ngFor="let item of navItems">
        <button (click)="onNavigationChange.emit(item)" mat-button>{{item.navItemName}}</button>
      </div>
      
      <ng-content></ng-content>
    </mat-toolbar>
  `
})
export class ToolbarComponent {
  @Input()  navItems: NavItem[];

  @Output() openMenu = new EventEmitter();

  @Output() onNavigationChange = new EventEmitter<CustomRoute>();
}
