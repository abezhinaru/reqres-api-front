import { Component, Input, Output, EventEmitter } from "@angular/core";
import UserFilter from '../models/user-filter';


@Component({
  selector: 'user-search',
  template: `    
    <button (click)="onSearchClickHandler(filter)">Search</button>
  `
})
export class UserSearch {
  @Input() filter: UserFilter = new UserFilter(1, 1);
  @Output() filterChange = new EventEmitter<UserFilter>();

  onSearchClickHandler(userFilter: UserFilter) {
    this.filterChange.emit(userFilter);
  }
}
