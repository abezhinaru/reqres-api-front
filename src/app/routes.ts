import {Route} from "@angular/router";

export interface CustomRoute extends Route {
  navItemName?: string;
  label?: string,
  icon?: string
}

export const appRoutes: CustomRoute[] = [
  { path: 'users', navItemName: 'Users', label: 'Users', icon: 'user'},
  { path: 'users/:id'}
];
