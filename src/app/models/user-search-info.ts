import User from './user';

export default class UserSearchInfo {
  page: number;
  per_page: number;
  total: number;
  total_pages: number;
  data: User[]
}
